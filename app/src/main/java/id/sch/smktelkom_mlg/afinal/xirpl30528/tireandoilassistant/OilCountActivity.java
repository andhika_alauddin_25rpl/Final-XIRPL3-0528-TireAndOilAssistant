package id.sch.smktelkom_mlg.afinal.xirpl30528.tireandoilassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class OilCountActivity extends AppCompatActivity {
    EditText odometer;
    Spinner JenKen;
    Spinner Lalin;
    Spinner MerkMot;
    Spinner MerkMob;
    Spinner JenOli;
    EditText JarTem;
    int hasil = 0;
    int JarTemKM;
    int Odo;
    int Pem;
    EditText Pemakaian;
    TextView tvTipKen;
    TextView tvHasilOdo;
    TextView tvHasilJartem;
    Button btnHitung;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oil_count);

        odometer = findViewById(R.id.editTextOdometer);
        JenKen = findViewById(R.id.spinnerJenKend);
        MerkMob = findViewById(R.id.spinnerMerkOliMob);
        MerkMot = findViewById(R.id.spinnerMerkOliMot);
        JenOli = findViewById(R.id.spinnerJenOli);
        Lalin = findViewById(R.id.spinnerLalin);
        JarTem = findViewById(R.id.etJarTem);
        Pemakaian = findViewById(R.id.etPemakaian);
        btnHitung = findViewById(R.id.buttonHitung);
        tvTipKen = findViewById(R.id.textViewTipKend);
        tvHasilOdo = findViewById(R.id.tvHasilOdo);
        tvHasilJartem = findViewById(R.id.tvHasilHari);
        db = FirebaseFirestore.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        JenKen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (JenKen.getSelectedItemPosition() == 1) {
                    tvTipKen.setVisibility(View.VISIBLE);
                    MerkMob.setVisibility(View.VISIBLE);
                    MerkMot.setVisibility(View.INVISIBLE);
                    Lalin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (Lalin.getSelectedItemPosition() == 1) {
                                hasil -= 500;
                            } else if (Lalin.getSelectedItemPosition() == 2) {
                                hasil -= 2000;
                            } else if (Lalin.getSelectedItemPosition() == 3) {
                                hasil -= 1500;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    JenOli.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (JenOli.getSelectedItemPosition() == 1) {
                                hasil += 2000;
                            } else if (JenOli.getSelectedItemPosition() == 2) {
                                hasil += 4000;
                            } else if (JenOli.getSelectedItemPosition() == 3) {
                                hasil += 3000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    MerkMob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (MerkMob.getSelectedItemPosition() == 1) {
                                hasil += 10000;
                            } else if (MerkMob.getSelectedItemPosition() == 2) {
                                hasil += 10000;
                            } else if (MerkMob.getSelectedItemPosition() == 3) {
                                hasil += 10000;
                            } else if (MerkMob.getSelectedItemPosition() == 4) {
                                hasil += 10000;
                            } else if (MerkMob.getSelectedItemPosition() == 5) {
                                hasil += 10000;
                            } else if (MerkMob.getSelectedItemPosition() == 6) {
                                hasil += 10000;
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else if (JenKen.getSelectedItemPosition() == 2) {
                    tvTipKen.setVisibility(View.VISIBLE);
                    MerkMob.setVisibility(View.INVISIBLE);
                    MerkMot.setVisibility(View.VISIBLE);
                    Lalin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (Lalin.getSelectedItemPosition() == 1) {
                                hasil -= 500;
                            } else if (Lalin.getSelectedItemPosition() == 2) {
                                hasil -= 2000;
                            } else if (Lalin.getSelectedItemPosition() == 3) {
                                hasil -= 1500;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    JenOli.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (JenOli.getSelectedItemPosition() == 1) {
                                hasil += 2000;
                            } else if (JenOli.getSelectedItemPosition() == 2) {
                                hasil += 4000;
                            } else if (JenOli.getSelectedItemPosition() == 3) {
                                hasil += 3000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    MerkMot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (MerkMot.getSelectedItemPosition() == 1) {
                                hasil += 10000;
                            } else if (MerkMot.getSelectedItemPosition() == 2) {
                                hasil += 10000;
                            } else if (MerkMot.getSelectedItemPosition() == 3) {
                                hasil += 10000;
                            } else if (MerkMot.getSelectedItemPosition() == 4) {
                                hasil += 10000;
                            } else if (MerkMot.getSelectedItemPosition() == 5) {
                                hasil += 10000;
                            } else if (MerkMot.getSelectedItemPosition() == 6) {
                                hasil += 10000;
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    tvTipKen.setVisibility(View.VISIBLE);
                    MerkMob.setVisibility(View.INVISIBLE);
                    MerkMot.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (odometer.getText().toString().matches("") || JarTem.getText().toString().matches("") || Pemakaian.getText().toString().matches("")) {
                    tvHasilOdo.setText("Masukkan Odometer, Jarak tempuh per km dan Masa pemakaian");
                    tvHasilJartem.setText("");
                } else {
                    Odo = Integer.parseInt(odometer.getText().toString());
                    JarTemKM = Integer.parseInt(JarTem.getText().toString());
                    Pem = Integer.parseInt(Pemakaian.getText().toString());
                    Odo = (Odo - (JarTemKM * Pem)) + hasil;
                    JarTemKM = (hasil / JarTemKM) - Pem;
                    if (Odo < 0 || JarTemKM < 0) {
                        tvHasilOdo.setText("Anda sudah melewati batas pemakaian oli");
                        tvHasilJartem.setText("");
                        hasil = 0;
                    } else {
                        tvHasilOdo.setText(Odo + " Km");
                        tvHasilJartem.setText(JarTemKM + " Hari");
                        hasil = 0;
                    }
                    Map<String, Object> data = new HashMap<>();
                    data.put("OilOdo", Odo);
                    data.put("OilKM", JarTemKM);
                    db.collection(user.getEmail()).document("Oil")
                            .set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(), "Sukses simpan data", Toast.LENGTH_LONG).show();
                                }
                            });

                    JenKen.setSelection(0);
                    Lalin.setSelection(0);
                    JenOli.setSelection(0);
                    MerkMob.setSelection(0);
                    MerkMot.setSelection(0);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(main);
    }
}
