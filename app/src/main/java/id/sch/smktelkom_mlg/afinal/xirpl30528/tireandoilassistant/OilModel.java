package id.sch.smktelkom_mlg.afinal.xirpl30528.tireandoilassistant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DK on 29/04/2018.
 */

public class OilModel {
    public String id;
    public double Olikm;
    public double Olihari;

    public OilModel() {

    }

    public OilModel(String id, double olikm, double olihari) {
        this.id = id;
        this.Olihari = olihari;
        this.Olikm = olikm;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getOlikm() {
        return Olikm;
    }

    public void setOlikm(double olikm) {
        Olikm = olikm;
    }

    public double getOlihari() {
        return Olihari;
    }

    public void setOlihari(double olihari) {
        Olihari = olihari;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("OilOdo", this.Olikm);
        result.put("OilKM", this.Olihari);

        return result;
    }
}
