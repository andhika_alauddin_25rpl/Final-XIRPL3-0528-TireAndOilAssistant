package id.sch.smktelkom_mlg.afinal.xirpl30528.tireandoilassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity {
    ProgressBar progressBar;
    boolean status = false;
    ImageView iv_user;
    TextView tv_nama, tv_hitungBanKm, tv_hitungBanHari, tv_hitungOliKm, tv_hitungOliHari;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    TextView logout;
    String bankm;
    String banhari;
    private FloatingActionButton fab;
    private FloatingActionButton fabTire;
    private FloatingActionButton fabOil;
    private boolean fabExpanded = false;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fab = findViewById(R.id.fab1);
        fabTire = findViewById(R.id.fabTire);
        fabOil = findViewById(R.id.fabOil);
        progressBar = findViewById(R.id.progressBar);
        iv_user = findViewById(R.id.iv_user);
        tv_nama = findViewById(R.id.tv_namaUser);
        tv_hitungBanKm = findViewById(R.id.tv_tampilHasilBanKm);
        tv_hitungBanHari = findViewById(R.id.tv_tampilHasilBanHari);
        tv_hitungOliKm = findViewById(R.id.tv_tampilHasilOliKm);
        tv_hitungOliHari = findViewById(R.id.tv_tampilHasilOliHari);
        db = FirebaseFirestore.getInstance();
        accessDB();
        if (user != null) {
        Glide.with(this).load(user.getPhotoUrl()).into(iv_user);
            tv_nama.setText(user.getDisplayName());
        }

        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {
                                       if (fabExpanded == true) {
                                           closeSubMenusFab();
                                       } else {
                                           openSubMenusFab();
                                       }
                                   }
                               }
        );

        fabTire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iLogin = new Intent(getApplicationContext(), TireCountActivity.class);
                startActivity(iLogin);
            }
        });

        fabOil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister = new Intent(getApplicationContext(), OilCountActivity.class);
                startActivity(iRegister);
            }
        });

        //Only main FAB is visible in the beginning
        closeSubMenusFab();


    }

    private void Logout() {
        AuthUI.getInstance()
                .signOut(getApplicationContext())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(login);
                    }
                });
    }

    private void closeSubMenusFab() {
        fabTire.setVisibility(View.INVISIBLE);
        fabOil.setVisibility(View.INVISIBLE);
        fabExpanded = false;
    }

    //Opens FAB submenus
    private void openSubMenusFab() {
        fabTire.setVisibility(View.VISIBLE);
        fabOil.setVisibility(View.VISIBLE);
        fabExpanded = true;
    }

    private void accessDB() {
        if (user != null) {
            progressBar.setVisibility(View.INVISIBLE);
            db.collection(user.getEmail()).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (final DocumentSnapshot doc : task.getResult()) {
                                    if (doc.exists()) {
                                        final DocumentReference documentReference = db.collection(user.getEmail()).document("Tire");
                                        final DocumentReference documentReference1 = db.collection(user.getEmail()).document("Oil");
                                        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(getApplicationContext(), "Data berhasil", Toast.LENGTH_SHORT).show();
                                                    DocumentSnapshot documentSnapshot = task.getResult();
                                                    TireModel tireModel = new TireModel();
                                                    final OilModel oilModel = new OilModel();
                                                    if (documentSnapshot.getDouble("TiresKM") != null || documentSnapshot.getDouble("TiresOdo") != null) {
                                                        tireModel.setBankm(documentSnapshot.getDouble("TiresOdo"));
                                                        tireModel.setBanhari(documentSnapshot.getDouble("TiresKM"));
                                                        tv_hitungBanHari.setText(tireModel.getBanhari() + " Hari");
                                                        tv_hitungBanKm.setText(tireModel.getBankm() + " Km");
                                                    } else {
                                                        tv_hitungBanHari.setText("0 Km");
                                                        tv_hitungBanKm.setText("0 Hari");
                                                    }
                                                    documentReference1.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                            DocumentSnapshot documentSnapshot1 = task.getResult();
                                                            if (documentSnapshot1.getDouble("OilOdo") != null || documentSnapshot1.getDouble("OilKM") != null) {
                                                                oilModel.setOlikm(documentSnapshot1.getDouble("OilKM"));
                                                                oilModel.setOlihari(documentSnapshot1.getDouble("OilOdo"));
                                                                tv_hitungOliKm.setText(oilModel.getOlikm() + " Km");
                                                                tv_hitungOliHari.setText(oilModel.getOlihari() + " Hari");
                                                            } else {
                                                                tv_hitungOliHari.setText("0 Hari");
                                                                tv_hitungOliKm.setText("0 Km");
                                                            }
                                                        }
                                                    });

                                                }
                                            }
                                        });
//                                        documentReference1.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                                            @Override
//                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                                                if (task.isSuccessful()) {
//                                                    Toast.makeText(getApplicationContext(), "Data berhasil", Toast.LENGTH_SHORT).show();
//                                                    DocumentSnapshot documentSnapshot = task.getResult();
//                                                    TireModel tireModel = new TireModel();
//                                                    OilModel oilModel = new OilModel();
//                                                    if (documentSnapshot.getDouble("TiresKM") != null || documentSnapshot.getDouble("TiresOdo") != null) {
//                                                        tireModel.setBankm(documentSnapshot.getDouble("TiresOdo"));
//                                                        tireModel.setBanhari(documentSnapshot.getDouble("TiresKM"));
//                                                        tv_hitungBanHari.setText(tireModel.getBanhari() + " Hari");
//                                                        tv_hitungBanKm.setText(tireModel.getBankm() + " Km");
//                                                    } else {
//                                                        tv_hitungBanHari.setText("0 Km");
//                                                        tv_hitungBanKm.setText("0 Hari");
//                                                    }
//                                                    if (documentSnapshot.getDouble("OilOdo") != null || documentSnapshot.getDouble("OilKM") != null) {
//                                                        oilModel.setOlikm(documentSnapshot.getDouble("OilKM"));
//                                                        oilModel.setOlihari(documentSnapshot.getDouble("OilOdo"));
//                                                        tv_hitungOliKm.setText(oilModel.getOlikm() + "Km");
//                                                        tv_hitungOliHari.setText(oilModel.getOlihari() + "Hari");
//                                                    } else {
//                                                        tv_hitungOliHari.setText("0 Hari");
//                                                        tv_hitungOliKm.setText("0 Km");
//                                                    }
//                                                }
//                                            }
//                                        });
                                    } else {
                                        tv_hitungBanHari.setText("0 Km");
                                        tv_hitungBanKm.setText("0 Hari");
                                        tv_hitungOliHari.setText("0 Hari");
                                        tv_hitungOliKm.setText("0 Km");
                                    }
                                }
                            }
                        }
                    });

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Logout();
    }
}