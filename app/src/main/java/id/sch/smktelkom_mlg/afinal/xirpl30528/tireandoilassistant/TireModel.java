package id.sch.smktelkom_mlg.afinal.xirpl30528.tireandoilassistant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DK on 29/04/2018.
 */

public class TireModel {

    public String id;
    public double bankm;
    public double banhari;


    public TireModel() {

    }

    public TireModel(String id, double bankm, double banhari) {
        this.id = id;
        this.bankm = bankm;
        this.banhari = banhari;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getBankm() {
        return bankm;
    }

    public void setBankm(double bankm) {
        this.bankm = bankm;
    }

    public double getBanhari() {
        return banhari;
    }

    public void setBanhari(double banhari) {
        this.banhari = banhari;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("TiresOdo", this.bankm);
        result.put("TiresKM", this.banhari);

        return result;
    }
}
