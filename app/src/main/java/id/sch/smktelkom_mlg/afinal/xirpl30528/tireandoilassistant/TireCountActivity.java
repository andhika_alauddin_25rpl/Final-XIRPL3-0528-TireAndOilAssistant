package id.sch.smktelkom_mlg.afinal.xirpl30528.tireandoilassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class TireCountActivity extends AppCompatActivity {

    EditText odometer;
    Spinner JenKen;
    Spinner TipMob;
    Spinner TipMot;
    Spinner JenBan;
    Spinner MerkMob;
    Spinner MerkMot;
    EditText JarTem;
    EditText Pemakaian;
    TextView tvTipKen;
    TextView tvHasilOdo;
    TextView tvHasilJarTem;
    TextView tvMerk;
    Button btnHitung;
    int hasil = 0;
    int Odo;
    int JarTemKM;
    int Pem;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tire_count);
        odometer = findViewById(R.id.editTextOdometer);
        JenKen = findViewById(R.id.spinnerJenKend);
        TipMob = findViewById(R.id.spinnerLalin);
        TipMot = findViewById(R.id.spinnerTipMot);
        JenBan = findViewById(R.id.spinnerJenBan);
        MerkMob = findViewById(R.id.spinnerMerkBanMob);
        MerkMot = findViewById(R.id.spinnerMerkBanMot);
        JarTem = findViewById(R.id.tvPemakaian);
        tvTipKen = findViewById(R.id.textViewTipKend);
        tvMerk = findViewById(R.id.textViewMerkBan);
        tvHasilOdo = findViewById(R.id.tvHasilOdo);
        tvHasilJarTem = findViewById(R.id.tvHasilJarTem);
        btnHitung = findViewById(R.id.buttonHitung);
        Pemakaian = findViewById(R.id.etPem);
        db = FirebaseFirestore.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        JenKen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (JenKen.getSelectedItemPosition() == 1) {
                    tvTipKen.setVisibility(View.VISIBLE);
                    TipMob.setVisibility(View.VISIBLE);
                    TipMot.setVisibility(View.INVISIBLE);
                    MerkMob.setVisibility(View.VISIBLE);
                    MerkMot.setVisibility(View.INVISIBLE);
                    tvMerk.setVisibility(View.VISIBLE);

                    TipMob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (TipMob.getSelectedItemPosition() == 1) {
                                hasil -= 2000;
                            } else if (TipMob.getSelectedItemPosition() == 2) {
                                hasil -= 4000;
                            } else if (TipMob.getSelectedItemPosition() == 3) {
                                hasil -= 3000;
                            } else if (TipMob.getSelectedItemPosition() == 4) {
                                hasil -= 4500;
                            } else if (TipMob.getSelectedItemPosition() == 5) {
                                hasil -= 6000;
                            } else if (TipMob.getSelectedItemPosition() == 6) {
                                hasil -= 6000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    JenBan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (JenBan.getSelectedItemPosition() == 1) {
                                hasil -= 5000;
                            } else if (JenBan.getSelectedItemPosition() == 2) {
                                hasil -= 8000;
                            } else if (JenBan.getSelectedItemPosition() == 3) {
                                hasil -= 7000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    MerkMob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (MerkMob.getSelectedItemPosition() == 1) {
                                hasil += 60000;
                            } else if (MerkMob.getSelectedItemPosition() == 2) {
                                hasil += 60000;
                            } else if (MerkMob.getSelectedItemPosition() == 3) {
                                hasil += 50000;
                            } else if (MerkMob.getSelectedItemPosition() == 4) {
                                hasil += 40000;
                            } else if (MerkMob.getSelectedItemPosition() == 5) {
                                hasil += 40000;
                            } else if (MerkMob.getSelectedItemPosition() == 6) {
                                hasil += 35000;
                            } else if (MerkMob.getSelectedItemPosition() == 7) {
                                hasil += 50000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                } else if (JenKen.getSelectedItemPosition() == 2) {
                    tvTipKen.setVisibility(View.VISIBLE);
                    TipMot.setVisibility(View.VISIBLE);
                    TipMob.setVisibility(View.INVISIBLE);
                    MerkMob.setVisibility(View.INVISIBLE);
                    MerkMot.setVisibility(View.VISIBLE);
                    tvMerk.setVisibility(View.VISIBLE);

                    TipMot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (TipMot.getSelectedItemPosition() == 1) {
                                hasil -= 3000;
                            } else if (TipMot.getSelectedItemPosition() == 2) {
                                hasil -= 2000;
                            } else if (TipMot.getSelectedItemPosition() == 3) {
                                hasil -= 3000;
                            } else if (TipMot.getSelectedItemPosition() == 4) {
                                hasil -= 4000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }

                    });
                    JenBan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (JenBan.getSelectedItemPosition() == 1) {
                                hasil -= 5000;
                            } else if (JenBan.getSelectedItemPosition() == 2) {
                                hasil -= 8000;
                            } else if (JenBan.getSelectedItemPosition() == 3) {
                                hasil -= 7000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    MerkMot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (MerkMot.getSelectedItemPosition() == 1) {
                                hasil += 15000;
                            } else if (MerkMot.getSelectedItemPosition() == 2) {
                                hasil += 15000;
                            } else if (MerkMot.getSelectedItemPosition() == 3) {
                                hasil += 20000;
                            } else if (MerkMot.getSelectedItemPosition() == 4) {
                                hasil += 20000;
                            } else if (MerkMot.getSelectedItemPosition() == 5) {
                                hasil += 15000;
                            } else if (MerkMot.getSelectedItemPosition() == 6) {
                                hasil += 10000;
                            } else if (MerkMot.getSelectedItemPosition() == 7) {
                                hasil += 20000;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    tvTipKen.setVisibility(View.VISIBLE);
                    TipMot.setVisibility(View.INVISIBLE);
                    TipMob.setVisibility(View.INVISIBLE);
                    MerkMot.setVisibility(View.INVISIBLE);
                    MerkMob.setVisibility(View.INVISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (odometer.getText().toString().matches("") || JarTem.getText().toString().matches("") || Pemakaian.getText().toString().matches("")) {
                    tvHasilOdo.setText("Masukkan Odometer, Jarak tempuh per km dan Masa pemakaian");
                    tvHasilJarTem.setText("");
                } else {
                    Odo = Integer.parseInt(odometer.getText().toString());
                    JarTemKM = Integer.parseInt(JarTem.getText().toString());
                    Pem = Integer.parseInt(Pemakaian.getText().toString());
                    Odo = (Odo - (JarTemKM * Pem)) + hasil;
                    JarTemKM = (hasil / JarTemKM) - Pem;
                    if (Odo < 0 || JarTemKM < 0) {
                        tvHasilOdo.setText("Anda sudah melewati batas pemakaian oli");
                        tvHasilJarTem.setText("");
                        hasil = 0;
                    } else {
                        tvHasilOdo.setText(Odo + " Km");
                        tvHasilJarTem.setText(JarTemKM + " Hari");
                        hasil = 0;
                    }
                    Map<String, Object> data = new HashMap<>();
                    data.put("TiresOdo", Odo);
                    data.put("TiresKM", JarTemKM);
                    db.collection(user.getEmail()).document("Tire")
                            .set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(), "Sukses simpan data", Toast.LENGTH_LONG).show();
                                }
                            });

                    JenKen.setSelection(0);
                    TipMob.setSelection(0);
                    TipMot.setSelection(0);
                    JenBan.setSelection(0);
                    MerkMob.setSelection(0);
                    MerkMot.setSelection(0);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(main);
    }

}
